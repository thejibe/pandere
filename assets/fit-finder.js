//Ready document
var $ = jQuery;
$(function() {
  //console.log('finder');
  
  var genderSet, swellSet, widthSet, bunSet;

  $(".next").click(function() {
    var $btn = $(this),
    $step = $btn.parents(".modal-body"),
    stepIndex = $step.index(),
    $pag = $(".modal-header span").eq(stepIndex);

    $step.addClass("animate-out");

    // animate the step in
    setTimeout(function() {
      $step
        .removeClass("animate-out is-showing")
        .next()
        .addClass("animate-in");
      $pag
        .removeClass("is-active")
        .next()
        .addClass("is-active");
    }, 600);

    // after the animation, adjust the classes
    setTimeout(function() {
      $step
        .next()
        .removeClass("animate-in")
        .addClass("is-showing");
    }, 1200);
  });

  $(".back").click(function() {
    var $btn = $(this),
      $step = $btn.parents(".modal-body"),
      stepIndex = $step.index(),
      $pag = $(".modal-header span").eq(stepIndex);

      $step.addClass("animate-out");

      // animate the step in
      setTimeout(function() {
        $step
          .removeClass("animate-out is-showing")
          .prev()
          .addClass("animate-in");
        $pag
          .removeClass("is-active")
          .prev()
          .addClass("is-active");
      }, 600);

      // after the animation, adjust the classes
      setTimeout(function() {
        $step
          .prev()
          .removeClass("animate-in")
          .addClass("is-showing");
      }, 1200);
  });

  //Gender Step 1
  $("#gender .button").click(function() {
    var radioGen = $("#gender input[name='gender']:checked").val();
    if (radioGen == "women") {
      $("#fitfinder").attr("data-gender", radioGen);
      $("#width .men").hide();
      $("#width .women").show();
      $("#width .men").removeClass("active-width");
      $("#width .women").addClass("active-width");
    }
    if (radioGen == "men") {
      $("#fitfinder").attr("data-gender", radioGen);
      $("#width .women").hide();
      $("#width .men").show();
      $("#width .women").removeClass("active-width");
      $("#width .men").addClass("active-width");
    }
    genderSet = radioGen;
    //Show Tooltip #1
    setTimeout(function() {
      $('#talk-one').addClass('show');
    }, 100);
  });

  //Swelling Step 2
  $("#swelling .button").click(function() {

    var radioSwell = $("#swelling input[name='swell']:checked").val();
    if (radioSwell) {
      $("#fitfinder").attr("data-swell", radioSwell);
    }
    swellSet = radioSwell;

  });

  //Width Step 3
  $("#width .button").click(function() {
    var radioValue = $(".active-width input[name='swell_opt']:checked").val();
    if (radioValue) {
      $("#fitfinder").attr("data-width", radioValue);
    }
    widthSet = radioValue;
    //Show Tooltip #1
    setTimeout(function() {
      $('#talk-two').addClass('show');
    }, 100);
  });

  //Bunions Step 4
  $("#bunion .button").click(function() {
    var radioBuns = $("#bunion input[name='bunion']:checked").val();
    if (radioBuns) {
      $("#fitfinder").attr("data-bunion", radioBuns);
    }
    bunSet = radioBuns;
    compute(genderSet, swellSet, widthSet, bunSet);
    console.log(genderSet, swellSet, widthSet, bunSet);
    //console.log(genderSet);
    //console.log(swellSet);
    //console.log(widthSet);
    //console.log(bunSet);
  });

  $(".set_again").click(function() {
    $(".modal-wrap")
      .removeClass("animate-up")
      .find(".modal-body")
      .first()
      .addClass("is-showing")
      .siblings()
      .removeClass("is-showing");

    $(".modal-header span")
      .first()
      .addClass("is-active")
      .siblings()
      .removeClass("is-active");
    $(this).hide();
  });

  $(".reset").click(function() {
    location.reload();
  });

  //Images
  var bolero_img = 'https://cdn.shopify.com/s/files/1/1976/5903/products/Bolero_Black-online_224x224_crop_center.jpeg?v=1564602756';
  var barista_img = 'https://cdn.shopify.com/s/files/1/1976/5903/products/1-pandere-barista-all-colors_224x224_crop_center.jpg?v=1562011347';
  var saturday_img = 'https://cdn.shopify.com/s/files/1/1976/5903/products/pandere-saturday-grey-1_224x224_crop_center.jpg?v=1566064633';
  var neo_img = 'https://cdn.shopify.com/s/files/1/1976/5903/products/pandere-neo-shoes-1_224x224_crop_center.jpg?v=1562011309';
  var rodeo_img = 'https://cdn.shopify.com/s/files/1/1976/5903/products/BordeauxRodeo_Black_online_256x224_crop_center.jpeg?v=1564602660';

  //Single Results
  var bolero =  $('<div class="bolero"><a href="https://www.pandereshoes.com/collections/the-worlds-most-stylish-expandable-shoe/products/bolero-shoe"><img src="'+bolero_img+'" alt="bolero shoe"/><h3>Bolero Shoe</h3></a></div>');

  var barista =  $('<div class="barista"><a href="https://www.pandereshoes.com/collections/the-worlds-most-stylish-expandable-shoe/products/pandere-barista-shoe"><img src="'+barista_img+'" alt="barista shoe"/><h3>Barista Shoe</h3></a></div>');

  var saturday =  $('<div class="saturday"><a href="https://www.pandereshoes.com/collections/the-worlds-most-stylish-expandable-shoe/products/saturday-shoe"><img src="'+saturday_img+'" alt="saturday shoe"/><h3>Saturday Shoe</h3></a></div>');

  var neo =  $('<div class="neo"><a href="https://www.pandereshoes.com/collections/the-worlds-most-stylish-expandable-shoe/products/pandere-neo-shoe"><img src="'+neo_img+'" alt="neo shoe"/><h3>Neo Shoe</h3></a></div>');

  var rodeo =  $('<div class="rodeo"><a href="https://www.pandereshoes.com/collections/the-worlds-most-stylish-expandable-shoe/products/rodeo"><img src="'+rodeo_img+'" alt="rodeo shoe"/><h3>Rodeo Shoe</h3></a></div>');

  function compute(genderSet, swellSet, widthSet, bunSet) {
    $("<div class='with-result'><p>This shoe accommodates "+swellSet+" swelling.</p><p>These shoes accommodates "+widthSet+" width.</p><p>This shoe is great for feet with "+bunSet+".</p></div>").insertBefore("#result_set");
    if (genderSet == "women") {
      //Women Start
      if (swellSet == "mild" || swellSet == "no") {
        //No & Mild Swelling
        if (widthSet == "AAA-A") {
          if (bunSet == "no-bunion") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p>We don't have shoes for narrow feet.<br>Our shoes are primarily geared toward accommodating swelling or medical conditions.</p></div>").appendTo(
              "#result_set"
            );
          } else {
            barista.appendTo('#result_set');
          }
        } else if (widthSet == "B-E") {
          $('.modal-body-step-7 .results').addClass('yes');
          if (bunSet == "no-bunion") {
            barista.appendTo('#result_set');
            rodeo.appendTo('#result_set');
            neo.appendTo('#result_set');
          } else {
            rodeo.appendTo('#result_set');
          }
        } else if (widthSet == "E-EEE") {
          $('.modal-body-step-7 .results').addClass('yes');
          if (bunSet == "no-bunion") {
            saturday.appendTo('#result_set');
          } else {
            saturday.appendTo('#result_set');
          }
        }
        else if (widthSet == "4E or larger") {
          $("#result_set").empty();
          $('#result-last').addClass('no-result');
          $("<div class='no_shoes'><p>We don't have a shoe that expands beyond a EEE right now.</p></div>").appendTo(
            "#result_set"
          );
        }
      } else if (swellSet == "moderate") {
        //No & Mild Swelling End
        //Moderate Swelling
        if (widthSet == "AAA-A") {
          if (bunSet == "no-bunion") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p> We're so sorry! We don't have a shoe for you yet.</p></div>").appendTo(
              "#result_set"
            );
          } else {
            $('.modal-body-step-7 .results').addClass('yes');
            barista.appendTo('#result_set');
          }
        } else if (widthSet == "B-E") {
          $('.modal-body-step-7 .results').addClass('yes');
          if (bunSet == "no-bunion") {
            rodeo.appendTo('#result_set');
            neo.appendTo('#result_set');
            saturday.appendTo('#result_set');
          } else {
            barista.appendTo('#result_set');
          }
        } else if (widthSet == "E-EEE") {
          $('.modal-body-step-7 .results').addClass('yes');
          if (bunSet == "no-bunion") {
            saturday.appendTo('#result_set');
          } else {
            saturday.appendTo('#result_set');
          }
        } 
        else if (widthSet == "4E or larger") {
          $("#result_set").empty();
          $('#result-last').addClass('no-result');
          $("<div class='no_shoes'><p>We don't have a shoe that expands beyond a EEE right now.</p></div>").appendTo(
            "#result_set"
          );
        }

      } else if (swellSet == "severe") {
        //Severe Swelling Start
        if (widthSet == "AAA-A") {
          if (bunSet == "no-bunion") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p> We're so sorry! We don't have a shoe for you yet.</p></div>").appendTo(
              "#result_set"
            );
          } else {
            $('.modal-body-step-7 .results').addClass('yes');
            barista.appendTo('#result_set');
          }
        } else if (widthSet == "B-E") {
          $('.modal-body-step-7 .results').addClass('yes');
          if (bunSet == "no-bunion") {
            saturday.appendTo('#result_set');
          } else {
            barista.appendTo('#result_set');
          }
        } else if (widthSet == "E-EEE") {
          if (bunSet == "no-bunion") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $('<div class="customer_service"><p>Customer Service message</p></div>').appendTo("#result_set");
          } else {
            barista.appendTo('#result_set');
          }
        } 
        else if (widthSet == "4E or larger") {
          $("#result_set").empty();
          $('#result-last').addClass('no-result');
          $("<div class='no_shoes'><p>We don't have a shoe that expands beyond a EEE right now.</p></div>").appendTo(
            "#result_set"
          );
        }
      } //Severe Swelling End
    } //Women End
     else {
      //Men Start
      if (swellSet == "mild" || swellSet == "no") {
        //No & Mild Swelling
        if (widthSet == "A-C") {
          if (bunSet == "no-bunion") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p>We don't have shoes for narrow feet.<br>Our shoes are primarily geared toward accommodating swelling or medical conditions.</p></div>").appendTo(
              "#result_set"
            );
          } else {  
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p> We're so sorry! We don't have a shoe for you yet.</p></div>").appendTo(
              "#result_set"
            );
          }
        } else if (widthSet == "C-E") {
          $('.modal-body-step-7 .results').addClass('yes');
          bolero.appendTo('#result_set');
        } else if (widthSet == "D-EE") {
          $('.modal-body-step-7 .results').addClass('yes');
          if (bunSet == "no-bunion") {
            saturday.appendTo('#result_set');
          } else {
            bolero.appendTo('#result_set');
            saturday.appendTo('#result_set');
          }
        } 
        else if (widthSet == "EEE or larger") {
          $("#result_set").empty();
          $('#result-last').addClass('no-result');
          $("<div class='no_shoes'><p>We don't have a shoe that expands beyond a EE right now.</p></div>").appendTo(
            "#result_set"
          );
        }
      } else if (swellSet == "moderate") {
        //Moderate Swelling
        if (widthSet == "A-C") {
          if (bunSet == "no-bunion") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p>We don't have shoes for narrow feet.<br>Our shoes are primarily geared toward accommodating swelling or medical conditions.</p></div>").appendTo(
              "#result_set"
            );
          } else {  
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p> We're so sorry! We don't have a shoe for you yet.</p></div>").appendTo(
              "#result_set"
            );
          }
        } else if (widthSet == "C-E") {
          $('.modal-body-step-7 .results').addClass('yes');
          saturday.appendTo('#result_set');
        } else if (widthSet == "D-EE") {
          $('.modal-body-step-7 .results').addClass('yes');
          saturday.appendTo('#result_set');
        } else if (widthSet == "EEE or larger") {
          $("#result_set").empty();
          $('#result-last').addClass('no-result');
          $("<div class='no_shoes'><p>We don't have a shoe that expands beyond a EE right now.</p></div>").appendTo(
            "#result_set"
          );
        }
      } else if (swellSet == "severe") {
        //Severe Swelling Start
        //Moderate Swelling
        if (widthSet == "A-C") {
          if (bunSet == "no-bunion") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p>We don't have shoes for narrow feet.<br>Our shoes are primarily geared toward accommodating swelling or medical conditions.</p></div>").appendTo(
              "#result_set"
            );
          } else { 
            $("#result_set").empty(); 
            $('#result-last').addClass('no-result');
            $("<div class='no_shoes'><p> We're so sorry! We don't have a shoe for you yet.</p></div>").appendTo(
              "#result_set"
            );
          }
        } else if (widthSet == "C-E") {
          $('.modal-body-step-7 .results').addClass('yes');
          saturday.appendTo('#result_set');
        } else if (widthSet == "D-EE") {
            $("#result_set").empty();
            $('#result-last').addClass('no-result');
            $('<div class="customer"><p>Customer Service message</p></div>').appendTo('#result_set');
        } else if (widthSet == "EEE or larger") {
          $("#result_set").empty();
          $('#result-last').addClass('no-result');
          $("<div class='no_shoes'><p>We don't have a shoe that expands beyond a EE right now.</p></div>").appendTo(
            "#result_set"
          );
        }
      } //Severe Swelling End
  }
}
});