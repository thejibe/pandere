# Elemental Labs (LMNT)

Short project intro here...

## Project Owner(s), QA

Francis Pilon

## Technical Project Owner(s)

Elvis Chan, elvis@thejibe.com

## The Jibe's Role


## Git Branching Model

This project follows the Gitflow branching model. See the [Version Control](https://docs.google.com/document/d/1Y8CWmAVAoK4HIIyXr9LCtKvgpMNzckvEIMzcTQVsobw/edit#heading=h.ge62apqanrav) section in the Developer Playbook.

This is a Shopify. Also see the [BigCommerce & Shopify Version Control](https://docs.google.com/document/d/1Y8CWmAVAoK4HIIyXr9LCtKvgpMNzckvEIMzcTQVsobw/edit#bookmark=id.bhwuc6c8wox) section in the Developer Playbook.

## Environment Versions

- Node (latest, no known version restrictions)
- Theme Kit (latest, no known version restrictions)

## Local Environment Setup


## Deployment Procedures


## Feature Documentation


## Technical TODO's


# The Jibe's Developer Playbook

The Developer Playbook is our general documentation for Developers at The Jibe. Whether this is your first or one hundredth project with us, dive in a familiarize yourself with our development concepts and conventions. This document is always evolving along with our development techniques and best practices.

[Developer Playbook](https://docs.google.com/document/d/1Y8CWmAVAoK4HIIyXr9LCtKvgpMNzckvEIMzcTQVsobw/edit)

_Please contribute back to this documentation. Contribution can take many forms: Add a new section, flagging content as old/dated or submitting a comment to initiate a conversation._
